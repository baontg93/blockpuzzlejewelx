﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogoAnimation : MonoBehaviour
{
    public Transform[] arrTextLogo;
    public float rangeMoveAnimation = 3f;
    public float timeMoveAnimation = 1f;
    public float delayTimeBetweenTexts = 0.5f;

    private int totalText;
    private Vector3[] arrSavePostion;
    private string keyTween;

    private void Awake()
    {
        totalText = arrTextLogo.Length;
        arrSavePostion = new Vector3[totalText];
        for (int index = 0; index < totalText; index++)
        {
            arrSavePostion[index] = arrTextLogo[index].localPosition;
        }
    }

    private void OnEnable()
    {
        KillTweenMoveAnimnation();

        StartCoroutine(RunAnimationLogo());
    }

    private void OnDisable()
    {
        KillTweenMoveAnimnation();
    }

    private void KillTweenMoveAnimnation()
    {
        StopAllCoroutines();

        List<Tween> listTween = DOTween.TweensById(keyTween);
        if (listTween != null)
        {
            foreach (var item in listTween)
            {
                item.Kill();
            }
        }

        for (int index = 0; index < totalText; index++)
        {
            arrTextLogo[index].localPosition = arrSavePostion[index];
        }
    }

    private IEnumerator RunAnimationLogo()
    {
        foreach (Transform tfText in arrTextLogo)
        {
            tfText.DOLocalMoveY(rangeMoveAnimation, timeMoveAnimation).SetLoops(-1, LoopType.Yoyo).SetId(keyTween);
            yield return new WaitForSeconds(delayTimeBetweenTexts);
        }
    }
}
